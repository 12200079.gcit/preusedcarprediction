from django.urls import path
from . import views

urlpatterns = [
    path('',views.Home,name='Home'),
    path('prediction/',views.Prediction,name='prediction'),
    path('price/',views.Price,name='price'),
  
]
